import { Injectable } from '@angular/core';
import journey from './../models/journey';

@Injectable()
export class JourneyService {
  currentPage: string; // We keep the current page that is set as a variable
  journeyId: string = '1'; // In case in the future we add more journeys we put it here so it can be dynamically created
  constructor() {}

  // Each jourey page set the page we currently are
  setCurrentPage(inputPage: string): void {
    this.currentPage = inputPage; // Assign in the global variable the current page
  }

  // Get the next page from the journey model based on the one you are now
  getNextPage(): string {
    const currentJourney = journey.find(jour => jour.id === this.journeyId); // Get the journey object from the model
    const indexOfCurrentPage = currentJourney.path.indexOf(this.currentPage); // Get the index of the current page
    this.currentPage = currentJourney.path[indexOfCurrentPage + 1]; // Update the current page with the one we took
    return currentJourney.path[indexOfCurrentPage + 1];
  }
}
