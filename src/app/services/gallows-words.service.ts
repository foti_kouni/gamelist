import { Injectable } from '@angular/core';
import wordsArray from '../models/word-data';

@Injectable()
export class GallowsWordsService {
  constructor() {}

  getRandomWord() {
    return wordsArray[Math.floor(Math.random() * wordsArray.length)];
  }
}
