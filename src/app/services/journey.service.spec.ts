import { TestBed, inject } from '@angular/core/testing';

import { JourneyService } from './journey.service';

describe('JourneyServiceService', () => {
  let service: JourneyService;
  let pathArray: string[] = ['intro', 'games', 'enjoy-meter', 'game'];
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JourneyService],
    });
  });
  beforeEach(() => {
    service = new JourneyService();
  });

  it('#setCurrentPage, should set a page', () => {
    for (let i = 0; i <= pathArray.length; i++) {
      service.setCurrentPage(pathArray[i]);
      expect(service.currentPage).toBe(pathArray[i]);
    }
  });

  it('#getNextPage, should bring next page from the model', () => {
    for (let i = 0; i <= pathArray.length - 1; i++) {
      service.currentPage = pathArray[i];
      expect(service.getNextPage()).toBe(pathArray[i + 1]);
    }
  });
});
