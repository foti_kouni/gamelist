import { TestBed } from '@angular/core/testing';

import { GallowsWordsService } from './gallows-words.service';
import wordsArray from '../models/word-data';

describe('GallowsWordsService', () => {
  let service: GallowsWordsService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GallowsWordsService],
    });
  });
  beforeEach(() => {
    service = new GallowsWordsService();
  });

  it('#getRandomWord, should take a word from the file', () => {
    expect(wordsArray).toContain(service.getRandomWord());
  });
});
