import { IGame } from './../types/games';
import { TestBed } from '@angular/core/testing';

import { GameDataService } from './game-data.service';

describe('GameDataService', () => {
  let service: GameDataService;
  let games: IGame[] = [
    {
      id: '1',
      name: 'gallows',
      component: 'gallows', //GallowsComponent,
    },
    {
      id: '2',
      name: 'tic-tac-toe',
      component: 'tic-tac-toe', // TicTacToeComponent,
    },
  ];
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameDataService],
    });
  });
  beforeEach(() => {
    service = new GameDataService();
  });

  it('#getGames, should get the name of the games object', () => {
    expect(service.getGames()).toContain('gallows', 'tic-tac-toe');
  });

  it('#setGamePlayed, should set the service variable', () => {
    service.setGamePlayed('gallows');
    expect(service.currentGame.id).toBe('1');
    service.setGamePlayed('tic-tac-toe');
    expect(service.currentGame.id).toBe('2');
  });

  it('#getGamePlayed, should get the the current game component', () => {
    for (let i = 0; i < games.length; i++) {
      service.currentGame = games[i];
      expect(service.getGamePlayed()).toBe(games[i].component);
    }
  });
});
