import { IGame } from './../types/games';
import { Injectable } from '@angular/core';
import games from '../models/games';

@Injectable()
export class GameDataService {
  currentGame: IGame; // Store the Game that is played

  constructor() {}

  // Get the list of the games from the model
  getGames(): string[] {
    return games.map(nameElement => nameElement.name);
  }

  // Set the game that is chosen in the intro
  setGamePlayed(inputGame: any): void {
    this.currentGame = games.find(gameName => gameName.name === inputGame); // Assign to the global variable the game that is chosen
  }

  // Get the game that is chosen
  getGamePlayed(): any {
    return this.currentGame.component;
  }
}
