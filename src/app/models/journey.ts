import { IJourney } from './../types/journey';

const journey: IJourney[] = [
  {
    id: '1',
    path: ['intro', 'games', 'enjoy-meter', 'game'],
  },
];

export default journey;
