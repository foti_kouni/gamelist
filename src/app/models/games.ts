import { SecretWordComponent } from './../components/pages/games/gallows/secret-word/secret-word.component';
import { TicTacToeComponent } from './../components/pages/games/tic-tac-toe/tic-tac-toe.component';
import { GallowsComponent } from './../components/pages/games/gallows/gallows.component';
import { IGame } from './../types/games';

const games: IGame[] = [
  {
    id: '1',
    name: 'gallows',
    component: 'gallows', //GallowsComponent,
  },
  {
    id: '2',
    name: 'tic-tac-toe',
    component: 'tic-tac-toe', // TicTacToeComponent,
  },
];

export default games;
