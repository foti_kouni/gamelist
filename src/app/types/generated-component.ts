export interface GeneratedComponentData {
  type: string; // A descriptive name for this component
  data: any; // A list of inputs and their values that need to be fed into the component
}
