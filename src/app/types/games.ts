export interface IGame {
  id: string; // game ID
  name: string; // game name
  component: any; //give the component that should be called
}
