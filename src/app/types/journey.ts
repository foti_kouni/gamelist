export interface IJourney {
  id: string; // unique id
  path: string[]; // the strings of the paths in this journey
}
