import { IGallowsState } from './../../../state-management/state/gallows.state';
import { TestStore } from './../../../../testStore';
import { GameDataService } from 'app/services/game-data.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnjoyMeterComponent } from './enjoy-meter.component';
import { Store } from '@ngrx/store';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import * as GallowsActions from './../../../state-management/actions/gallows.action';
import * as TicTacToeActions from './../../../state-management/actions/tic-tac-toe.action';
import { ITicTacToeState } from 'app/state-management/state/tic-tac-toe.state';

describe('EnjoyMeterComponent', () => {
  let component: EnjoyMeterComponent;
  let fixture: ComponentFixture<EnjoyMeterComponent>;
  let gallowsStore: TestStore<IGallowsState>;
  let tictactoeStore: TestStore<ITicTacToeState>;

  let expectedData = [
    {
      id: '1',
      name: 'gallows',
      component: 'gallows', //GallowsComponent,
    },
  ];

  let mockGameService = <GameDataService>{
    getGamePlayed: () => {
      return expectedData.map(nameElement => nameElement.name);
    },
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EnjoyMeterComponent],
      providers: [
        { provide: GameDataService, useValue: mockGameService },
        { provide: Store, useClass: TestStore },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    gallowsStore = TestBed.get(Store);
    tictactoeStore = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnjoyMeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize game on init', () => {
    expect(component.playedGame).toContain('gallows');
  });

  it('should dispatch the gallows game', () => {
    component.playedGame = 'gallows';
    let spyDispatch = spyOn(gallowsStore, 'dispatch');
    component.enjoyDay = 1;
    component.enjoyGame = 1;
    component.suggestGame = 1;
    fixture.destroy();
    expect(spyDispatch).toHaveBeenCalledWith(
      new GallowsActions.SetEnjoy({
        yourDay: 1,
        game: 1,
        suggestion: 1,
      })
    );
  });

  it('should dispatch the tic tac toe game', () => {
    component.playedGame = 'tic-tac-toe';
    let spyDispatch = spyOn(tictactoeStore, 'dispatch');
    component.enjoyDay = 1;
    component.enjoyGame = 1;
    component.suggestGame = 1;
    fixture.destroy();
    expect(spyDispatch).toHaveBeenCalledWith(
      new TicTacToeActions.SetTEnjoy({
        yourDay: 1,
        game: 1,
        suggestion: 1,
      })
    );
  });

  it('should call rate game on radio button click', () => {
    let spyClickButton = spyOn(component, 'rateGame');
    let button = fixture.debugElement.nativeElement.querySelector('input');
    button.click();
    expect(spyClickButton).toHaveBeenCalled();
  });

  it('should update the enjoy values', () => {
    component.rateGame('Did you enjoy your day today?', 'No way');
    expect(component.enjoyDay).toEqual(1);
    component.rateGame('Did you enjoy the game?', 'Maybe');
    expect(component.enjoyGame).toEqual(3);
    component.rateGame('Would you recommend the game to a friend?', 'For sure');
    expect(component.suggestGame).toEqual(5);
  });
});
