import { GameDataService } from './../../../services/game-data.service';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { GallowsDataStore } from 'app/state-management/state/gallows.state';
import * as GallowsActions from './../../../state-management/actions/gallows.action';
import * as TicTacToeActions from './../../../state-management/actions/tic-tac-toe.action';
import { TicTacToeDataStore } from 'app/state-management/state/tic-tac-toe.state';

@Component({
  selector: 'app-enjoy-meter',
  templateUrl: './enjoy-meter.component.html',
  styleUrls: ['./enjoy-meter.component.scss'],
})
export class EnjoyMeterComponent implements OnInit {
  questions = [
    // questions that we will ask the visitors
    'Did you enjoy your day today?',
    'Did you enjoy the game?',
    'Would you recommend the game to a friend?',
  ];
  rating = ['No way', 'No', 'Maybe', 'Yes', 'For sure']; // The rating for each question
  enjoyDay: number = 0; // Take the rating of the first question
  enjoyGame: number = 0; // Take the rating of the second question
  suggestGame: number = 0; // Take the rating of the third question
  playedGame: string; // Send for the game played the data to the respective store

  constructor(
    private gameData: GameDataService,
    private gallowsStore: Store<GallowsDataStore>,
    private tictactoeStore: Store<TicTacToeDataStore>
  ) {}

  ngOnInit() {
    this.playedGame = this.gameData.getGamePlayed(); // Grab the game that was played to go to the following store
  }

  // On destroy send the ratings to the store for the game that was played
  ngOnDestroy() {
    if (this.playedGame === 'gallows') {
      this.gallowsStore.dispatch(
        new GallowsActions.SetEnjoy({
          yourDay: this.enjoyDay,
          game: this.enjoyGame,
          suggestion: this.suggestGame,
        })
      );
    } else if (this.playedGame === 'tic-tac-toe') {
      this.tictactoeStore.dispatch(
        new TicTacToeActions.SetTEnjoy({
          yourDay: this.enjoyDay,
          game: this.enjoyGame,
          suggestion: this.suggestGame,
        })
      );
    }
  }

  // Depending on which radio button was clicked give the responding rating
  rateGame(inputQuestion: string, inputRating: string) {
    // Check the different questions from the array
    switch (inputQuestion) {
      case this.questions[0]:
        this.enjoyDay = this.rating.indexOf(inputRating) + 1;
        break;
      case this.questions[1]:
        this.enjoyGame = this.rating.indexOf(inputRating) + 1;
        break;
      case this.questions[2]:
        this.suggestGame = this.rating.indexOf(inputRating) + 1;
        break;
    }
  }
}
