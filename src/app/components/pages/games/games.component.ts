import { GameDataService } from './../../../services/game-data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit {
  gamePlayed: string; // Store the game that should be called
  constructor(
    private gameData: GameDataService // injection for the game that will be played to call the respective component
  ) {}

  ngOnInit() {
    this.gamePlayed = this.gameData.getGamePlayed(); // Grab the game from the service
  }
}
