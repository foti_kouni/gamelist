import { IGallowsState } from './../../../../../state-management/state/gallows.state';
import { TestStore } from './../../../../../../testStore';
import { GallowsWordsService } from './../../../../../services/gallows-words.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecretWordComponent } from './secret-word.component';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/observable/of';
import * as GallowsActions from './../../../../../state-management/actions/gallows.action';

describe('SecretWordComponent', () => {
  let component: SecretWordComponent;
  let fixture: ComponentFixture<SecretWordComponent>;
  let gallowsStore: TestStore<IGallowsState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [SecretWordComponent],
      providers: [GallowsWordsService, { provide: Store, useClass: TestStore }],
    }).compileComponents();
    gallowsStore = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretWordComponent);
    component = fixture.componentInstance;
    component.events = Observable.of();
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the values', () => {
    expect(component.chosenWord).not.toBeNull();
    expect(component.chosenWordLength).not.toBeNull();
    expect(component.wordShownArray).toContain('_');
    let spyDisp = spyOn(component, 'dispatchWord');
    let spyFatherSub = spyOn(component, 'setUpFatherSubscription');
    component.ngOnInit();
    expect(spyDisp).toHaveBeenCalled();
    expect(spyFatherSub).toHaveBeenCalled();
  });

  it('should destroy', () => {
    let spyOnDest = spyOn(component, 'ngOnDestroy');
    //let spyOnUnsub = spyOn(component.eventSubscription, 'unsubscribe');
    fixture.destroy();
    expect(spyOnDest).toHaveBeenCalled();
    //expect(spyOnUnsub).toHaveBeenCalled();
  });

  it('should call check letters on father subscription', () => {
    let spyCheckLetters = spyOn(component, 'checkLetters');
    component.events = Observable.of('te');
    component.setUpFatherSubscription();
    expect(spyCheckLetters).toHaveBeenCalled();
  });

  it('should dispatch the word', () => {
    let spyDispatch = spyOn(gallowsStore, 'dispatch');
    component.dispatchWord();
    expect(spyDispatch).toHaveBeenCalledWith(
      new GallowsActions.AddChosenWord({
        word: component.chosenWord,
      })
    );
  });

  it('should check letters', () => {
    component.chosenWord = 'abraKadabra';
    component.checkLetters('a');
    expect(component.wordShownArray).toContain('a');
    component.checkLetters('t');
    expect(component.wordShownArray).not.toContain('t');
  });

  it('should dispatch the end of the game', () => {
    let spyDispatch = spyOn(gallowsStore, 'dispatch');

    component.dispatchEndGame();
    expect(spyDispatch).toHaveBeenCalledWith(
      new GallowsActions.EndGame({
        gameEnded: true,
      })
    );
  });
});
