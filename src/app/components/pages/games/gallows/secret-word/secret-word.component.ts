import { Subscription } from 'rxjs/Subscription';
import { GallowsWordsService } from './../../../../../services/gallows-words.service';
import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { GallowsDataStore } from '../../../../../state-management/state/gallows.state';
import * as GallowsActions from './../../../../../state-management/actions/gallows.action';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-secret-word',
  templateUrl: './secret-word.component.html',
  styleUrls: ['./secret-word.component.scss'],
})
export class SecretWordComponent implements OnInit, OnDestroy {
  @Input()
  events: Observable<string>; // Observable  to grab the event and the letter from father

  @Output()
  winMessage: EventEmitter<string> = new EventEmitter<string>(); // Emit the win state

  chosenWord: string; // The word that is chosen from our lexicon
  chosenWordLength: number; // The length of the chosen word
  wordShownArray: string[]; // The word with the characters that will be shown
  eventSubscription: Subscription; // Subscription for the click event
  initialShownCharacter: string = '_'; // The character that the array will be initialized

  constructor(
    private wordData: GallowsWordsService, // Inject the service to access the lexicon
    private gallowsStore: Store<GallowsDataStore> // Inject the store to dispatch actions
  ) {}

  ngOnInit() {
    this.chosenWord = this.wordData.getRandomWord(); // Grab the chosen word using the service
    this.chosenWordLength = this.chosenWord.length; // Calculate the length of the word
    this.wordShownArray = new Array(this.chosenWordLength).fill('_'); // Initialize the array with underscore

    this.dispatchWord();
    this.setUpFatherSubscription();
  }

  ngOnDestroy() {
    this.eventSubscription.unsubscribe();
  }

  // start subscription to connect with father component for the click events
  setUpFatherSubscription() {
    // Connect with father component and when a click event occurs call checkLetters()
    this.eventSubscription = this.events.subscribe(letter =>
      this.checkLetters(letter)
    );
  }

  // Send the chosen word to the Gallows Store
  dispatchWord() {
    this.gallowsStore.dispatch(
      // Send the word to the store
      new GallowsActions.AddChosenWord({
        word: this.chosenWord,
      })
    );
  }

  // Check if the letter that was pressed in brother component occurs in the chosenWord
  checkLetters(letter: string) {
    let occurrence: number = 0; // Keep track the index of the letter in the word

    // Check if letter exists in the word
    if (this.chosenWord.indexOf(letter) > -1) {
      // Make the word array and check each occurrence for all letters
      Array.from(this.chosenWord).forEach(element => {
        if (element === letter) {
          // Replace the underscore array with the letters to be shown
          this.wordShownArray[occurrence] = element;
        }
        occurrence += 1; // increase the letter index
      });
    }

    if (this.wordShownArray.indexOf(this.initialShownCharacter) === -1) {
      this.winMessage.emit(this.chosenWord); // Send the word to father component
      this.dispatchEndGame();
    }
  }

  // Send the end state to the Gallows Store
  dispatchEndGame() {
    this.gallowsStore.dispatch(
      // Send the word to the store
      new GallowsActions.EndGame({
        gameEnded: true,
      })
    );
  }
}
