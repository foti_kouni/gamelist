import {
  GallowsDataStore,
  IGallowsState,
} from './../../../../../state-management/state/gallows.state';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import * as GallowsActions from './../../../../../state-management/actions/gallows.action';

@Component({
  selector: 'app-hangman',
  templateUrl: './hangman.component.html',
  styleUrls: ['./hangman.component.scss'],
})
export class HangmanComponent implements OnInit {
  @Output()
  lossMessage: EventEmitter<string> = new EventEmitter<string>(); // Emit the win state

  LettersStateSubscription: Subscription; // The subscription variable
  falseLetters: string[] = []; // Put all the letters that does not exist in the word
  gallowsState: IGallowsState; // Store the state of gallows store

  constructor(private gallowsStore: Store<GallowsDataStore>) {}

  ngOnInit() {
    this.setUpSubscription(); // call the subscription function to start subscribing to the store
  }

  ngOnDestroy() {
    this.LettersStateSubscription.unsubscribe(); // don't forget to unsubscribe
  }

  // Subscribe to the gallows store
  setUpSubscription() {
    this.LettersStateSubscription = this.gallowsStore
      .select('gallowsData')
      .subscribe(state => {
        this.gallowsState = state;
        this.checkFalseLetters(); // call the function to check the errors
      });
  }

  // In each letter clicked we check if it didn't exist in the word and put it in the relevant array
  checkFalseLetters() {
    // Check if there are elements in the letters array and if the last letter that was just entered does not exist in the word
    if (
      this.gallowsState.letters.length > 0 &&
      !this.gallowsState.word.includes(
        this.gallowsState.letters[this.gallowsState.letters.length - 1]
      )
    ) {
      // Push it in the false letters array
      this.falseLetters.push(
        this.gallowsState.letters[this.gallowsState.letters.length - 1]
      );

      // Dispatch the end game action to store and emit it to father
      if (this.falseLetters.length > 8 && !this.gallowsState.gameEnded) {
        this.lossMessage.emit(this.gallowsState.word);
        this.dispatchEndGame();
      }
    }
  }

  // Send the end state to the Gallows Store
  dispatchEndGame() {
    this.gallowsStore.dispatch(
      // Send the word to the store
      new GallowsActions.EndGame({
        gameEnded: true,
      })
    );
  }
}
