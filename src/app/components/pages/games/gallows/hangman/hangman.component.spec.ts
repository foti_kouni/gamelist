import { Store } from '@ngrx/store';
import { IGallowsState } from './../../../../../state-management/state/gallows.state';
import { TestStore } from './../../../../../../testStore';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as GallowsActions from './../../../../../state-management/actions/gallows.action';

import { HangmanComponent } from './hangman.component';

describe('HangmanComponent', () => {
  let component: HangmanComponent;
  let fixture: ComponentFixture<HangmanComponent>;
  let gallowsStore: TestStore<IGallowsState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HangmanComponent],
      providers: [{ provide: Store, useClass: TestStore }],
    }).compileComponents();
    gallowsStore = TestBed.get(Store);
    gallowsStore.setState({
      word: '',
      letters: [],
      gameEnded: false,
      yourDay: 0,
      game: 0,
      suggestion: 0,
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HangmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should call to set up a subscription on init', () => {
    let spySetUp = spyOn(component, 'setUpSubscription');
    component.ngOnInit();
    expect(spySetUp).toHaveBeenCalled();
  });

  it('should unsubscribe on destroy', () => {
    let spyDestroy = spyOn(component, 'ngOnDestroy');
    // let spyUnsubscribe = spyOn(
    //   component.LettersStateSubscription,
    //   'unsubscribe'
    // );
    fixture.destroy();
    expect(spyDestroy).toHaveBeenCalled();
    //expect(spyUnsubscribe).toHaveBeenCalled();
  });

  it('should call check false letters on set up subscription', () => {
    let spyCheckFalse = spyOn(component, 'checkFalseLetters');
    component.setUpSubscription();
    expect(spyCheckFalse).toHaveBeenCalled();
  });

  it('should update the false letters array if the letter does not exist in the word', () => {
    component.gallowsState.word = 'abraKadabra';
    component.gallowsState.letters = ['a'];
    component.checkFalseLetters();
    expect(component.falseLetters).not.toContain('a');
    component.gallowsState.letters = ['c'];
    component.checkFalseLetters();
    expect(component.falseLetters).toContain('c');
  });

  it('should call the dispatching end of the game and emit the word', () => {
    component.falseLetters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'];
    component.gallowsState.gameEnded = false;
    component.gallowsState.word = 'abraKadabra';
    component.gallowsState.letters = ['c'];
    let spyEmit = spyOn(component.lossMessage, 'emit');
    let spyEnd = spyOn(component, 'dispatchEndGame');
    component.checkFalseLetters();
    expect(spyEmit).toHaveBeenCalledWith('abraKadabra');
    expect(spyEnd).toHaveBeenCalled();
  });

  it('should dispatch that the game ended', () => {
    let spyDispatch = spyOn(gallowsStore, 'dispatch');
    component.dispatchEndGame();
    expect(spyDispatch).toHaveBeenCalledWith(
      new GallowsActions.EndGame({
        gameEnded: true,
      })
    );
  });
});
