import { Subscription } from 'rxjs/Subscription';
import {
  GallowsDataStore,
  initialState,
} from './../../../../state-management/state/gallows.state';
import { Store } from '@ngrx/store';
import { JourneyService } from './../../../../services/journey.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as GallowsActions from './../../../../state-management/actions/gallows.action';

@Component({
  selector: 'app-gallows',
  templateUrl: './gallows.component.html',
  styleUrls: ['./gallows.component.scss'],
})
export class GallowsComponent implements OnInit {
  eventSubject: Subject<string> = new Subject<string>(); // Subject string to pass the data to child component
  winState: boolean = false; // Show or not the win message component
  lossState: boolean = false; // Show or not the loss message component
  winString: string; // The string that will be shown by the component for the win
  lossString: string; // The string that the loss component will show
  continue: string = 'Continue'; // The text that the nav button should show
  nextPage: string; // The page that the nav button should direct to
  answeredEnjoyQuestions: boolean = false; // Check if it has already answered the survey
  surveyDoneSubscription: Subscription; // The subscriber for the store

  constructor(
    private journeyData: JourneyService,
    private gallowsStore: Store<GallowsDataStore>
  ) {}

  ngOnInit() {
    this.nextPage = this.journeyData.getNextPage();
    this.setUpSurveySubscription();
  }

  ngOnDestroy() {
    this.surveyDoneSubscription.unsubscribe();
  }

  // Set the subscription to grab the survey
  setUpSurveySubscription() {
    this.surveyDoneSubscription = this.gallowsStore
      .select('gallowsData')
      .subscribe(state => {
        state.suggestion > 0
          ? (this.answeredEnjoyQuestions = true)
          : (this.answeredEnjoyQuestions = false);
      });
  }

  // Pass data to child component in each click
  letterClicked(inputLetter: string) {
    this.eventSubject.next(inputLetter);
  }

  // In case of win state pass the respective variables
  enableWinState(inputWord: string) {
    this.winState = true; // Show the winning message
    this.winString =
      'Congratulations you won Gallows game, you found the word ' + inputWord;
    this.goToIntro();
  }

  // In case of losing show the respective messages
  enableLossState(inputWord: string) {
    this.lossState = true; // Show the losing message
    this.lossString = 'Unfortunately, you lost... The word was ' + inputWord;
    this.goToIntro();
  }

  // Set the next page to the introduction page
  goToIntro() {
    if (this.answeredEnjoyQuestions) {
      this.nextPage = 'intro';
    }
  }

  // In resetting game send the initial states we want
  resetGame() {
    this.gallowsStore.dispatch(
      new GallowsActions.ResetGame({
        word: initialState.word,
        letters: initialState.letters,
        gameEnded: initialState.gameEnded,
      })
    );
  }
}
