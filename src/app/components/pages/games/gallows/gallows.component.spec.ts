import { TestStore } from './../../../../../testStore';
import {
  IGallowsState,
  initialState,
} from './../../../../state-management/state/gallows.state';
import { Store } from '@ngrx/store';
import { JourneyService } from './../../../../services/journey.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GallowsComponent } from './gallows.component';
import { GallowsWordsService } from 'app/services/gallows-words.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import * as GallowsActions from './../../../../state-management/actions/gallows.action';

describe('GallowsComponent', () => {
  let component: GallowsComponent;
  let fixture: ComponentFixture<GallowsComponent>;
  let gallowsStore: TestStore<IGallowsState>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [GallowsComponent],
      providers: [
        JourneyService,
        { provide: Store, useClass: TestStore },
        GallowsWordsService,
      ],
    }).compileComponents();
    gallowsStore = TestBed.get(Store);
    gallowsStore.setState({
      word: '',
      letters: [],
      gameEnded: false,
      yourDay: 0,
      game: 0,
      suggestion: 0,
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GallowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should set subscription', () => {
    expect(component.answeredEnjoyQuestions).toEqual(false);
    gallowsStore.setState({
      word: '',
      letters: [],
      gameEnded: false,
      yourDay: 0,
      game: 0,
      suggestion: 1,
    });
    expect(component.answeredEnjoyQuestions).toEqual(true);
  });

  it('should enable win state', () => {
    let spyIntroCalled = spyOn(component, 'goToIntro');
    component.enableWinState('test');
    expect(component.winState).toBe(true);
    expect(component.winString).toBe(
      'Congratulations you won Gallows game, you found the word test'
    );
    expect(spyIntroCalled).toHaveBeenCalled();
  });

  it('should enable loss state', () => {
    let spyIntroCalled = spyOn(component, 'goToIntro');
    component.enableLossState('test');
    expect(component.lossState).toBe(true);
    expect(component.lossString).toBe(
      'Unfortunately, you lost... The word was test'
    );
    expect(spyIntroCalled).toHaveBeenCalled();
  });

  it('should set as next page the intro depending on the answered enjoy questions', () => {
    component.nextPage = '';
    component.goToIntro();
    expect(component.nextPage).not.toBe('intro');
    component.answeredEnjoyQuestions = true;
    component.goToIntro();
    expect(component.nextPage).toBe('intro');
  });

  it('should dispatch the data of the game', () => {
    let spyDisp = spyOn(gallowsStore, 'dispatch');
    initialState.word = 'test';
    initialState.letters = ['test'];
    initialState.gameEnded = false;
    component.resetGame();
    expect(spyDisp).toHaveBeenCalledTimes(1);
    expect(spyDisp).toHaveBeenCalledWith(
      new GallowsActions.ResetGame({
        word: 'test',
        letters: ['test'],
        gameEnded: false,
      })
    );
  });
});
