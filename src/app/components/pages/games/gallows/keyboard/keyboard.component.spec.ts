import { IGallowsState } from './../../../../../state-management/state/gallows.state';
import { TestStore } from './../../../../../../testStore';
import { Store } from '@ngrx/store';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as GallowsActions from './../../../../../state-management/actions/gallows.action';

import { KeyboardComponent } from './keyboard.component';

describe('KeyboardComponent', () => {
  let component: KeyboardComponent;
  let fixture: ComponentFixture<KeyboardComponent>;
  let gallowsStore: TestStore<IGallowsState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyboardComponent],
      providers: [{ provide: Store, useClass: TestStore }],
    }).compileComponents();
    gallowsStore = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should call end game observable on init', () => {
    let spyEndGame = spyOn(component, 'setUpEndGameObservable');
    component.ngOnInit();
    expect(spyEndGame).toHaveBeenCalled();
  });

  it('should call pipe for the gallows store', () => {
    let spyPipe = spyOn(gallowsStore, 'pipe');
    component.setUpEndGameObservable();
    expect(spyPipe).toHaveBeenCalled();
  });

  it('should check the letters that are pressed, dispatch and emit', () => {
    expect(component.lettersChecked).not.toContain(true);
    let spyEmit = spyOn(component.letterClicked, 'emit');
    let spyDispatch = spyOn(gallowsStore, 'dispatch');
    component.checkLetter('A', 1);
    expect(component.lettersChecked).toContain(true);
    expect(spyEmit).toHaveBeenCalledWith('a');
    expect(spyDispatch).toHaveBeenCalledWith(
      new GallowsActions.AddSelectedLetter({
        letter: 'a',
      })
    );
  });
});
