import { Observable } from 'rxjs/Observable';
import * as GallowsActions from './../../../../../state-management/actions/gallows.action';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { GallowsDataStore } from '../../../../../state-management/state/gallows.state';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss'],
})
export class KeyboardComponent implements OnInit {
  @Output()
  letterClicked: EventEmitter<string> = new EventEmitter<string>(); // Send the button click events

  endGame$: Observable<boolean>; // Observable for the end state
  stopClicks: boolean = false; // Stop the clicks if game ends

  lettersKeyboard: string[] = [
    // Contain all the letters that should be shown
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
  ];
  lettersChecked = new Array(this.lettersKeyboard.length).fill(false); // Contains if a letter is already pressed

  constructor(private gallowsStore: Store<GallowsDataStore>) {}

  ngOnInit() {
    this.setUpEndGameObservable(); // Set up the subscription
  }

  ngOnDestroy() {}

  // Set the subscription to grab the end state
  setUpEndGameObservable() {
    this.endGame$ = this.gallowsStore.pipe(
      select(state => state.gallowsData.gameEnded)
    );
  }

  // Check a letter that is pressed
  checkLetter(inputLetter: string, inputIndex: number) {
    this.lettersChecked[inputIndex] = true; // Assign to the respected letter of the array the true value

    // Send the letter that was pressed to the state
    this.gallowsStore.dispatch(
      new GallowsActions.AddSelectedLetter({
        letter: inputLetter.toLowerCase(),
      })
    );

    // Emit the click to father with the letter clicked
    this.letterClicked.emit(inputLetter.toLowerCase());
  }
}
