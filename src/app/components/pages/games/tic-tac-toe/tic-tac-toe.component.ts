import { TicTacToeDataStore } from './../../../../state-management/state/tic-tac-toe.state';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { JourneyService } from './../../../../services/journey.service';
import { Component, OnInit } from '@angular/core';
import * as TicTacToeActions from './../../../../state-management/actions/tic-tac-toe.action';

@Component({
  selector: 'app-tic-tac-toe',
  templateUrl: './tic-tac-toe.component.html',
  styleUrls: ['./tic-tac-toe.component.scss'],
})
export class TicTacToeComponent implements OnInit {
  cellNumbersArray: number[][] = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]; // Matrix for the cells component call
  cellSignsArray: string[] = new Array(9).fill(''); // In each place there is the sign that should have each cell
  winningArray: number[][] = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ]; // Contains all the winning states
  sign = 'X'; // The sign that should be written in each cell place
  gameState = 'play'; // The condition of the game;
  winCells: boolean[] = new Array(9).fill(false); // Array which will represent the win cells for the ng Class
  round: number = 0; // The round that increases after each time a player places his sign
  textShown: string; // The text that will be shown in the text component
  playerText: string = ' is playing now.'; // The text that will follow the player
  continue = 'Continue'; // The text for the continue button
  nextPage: string; // The page that the nav button should direct to
  answeredEnjoyQuestions: boolean = false; // Check if it has already answered the survey
  surveyDoneSubscription: Subscription; // The subscriber for the store
  winTriad: number[]; // Store the actual triad which contains the winning cells

  constructor(
    private journeyData: JourneyService,
    private tictactoeStore: Store<TicTacToeDataStore>
  ) {}

  ngOnInit() {
    this.setPlayerText(); // Show the starting player message
    this.nextPage = this.journeyData.getNextPage(); // Check the next page
    this.setUpSurveySubscription();
  }

  ngOnDestroy() {
    this.surveyDoneSubscription.unsubscribe();
  }

  // The main function after a sign was placed that will increase the round, change the state and call the other functions
  clickCell(inputCellNumber: number) {
    this.cellSignsArray[inputCellNumber] = this.sign; // Put the sign in the respective place in the array
    this.round++; // Increase the round number
    // Check if there are more than four signs in the array and call the respective function
    if (this.round > 4) {
      this.checkWinState(); // Check if a player won
      if (this.round === 9 && this.gameState === 'play') {
        this.gameState = 'draw'; // Set the game to draw if nobody won and it's the last round
      }
    }
    this.sign === 'X' ? (this.sign = 'O') : (this.sign = 'X'); // Change the sign
    this.setPlayerText(); // Check the message to be shown
  }

  // Depending on the state of the game construct and show each message
  setPlayerText() {
    if (this.gameState === 'win') {
      this.sign === 'X' ? (this.sign = 'O') : (this.sign = 'X'); // Change the sign
      this.textShown = 'Well done ' + this.sign + ' you won!';
      this.goToIntro(); // If we won check next page for the nav button
    } else if (this.gameState === 'draw') {
      this.textShown = 'You are both really good! The game was a draw';
      this.goToIntro(); // If we won check next page for the nav button
    } else {
      this.textShown = this.sign + this.playerText; // Default text for the next player
    }
  }

  // Based on the array with the sign calculate if there is a win state
  checkWinState() {
    let signArray: number[] = []; // Contains the positions that the current sign exist
    // Loop through the items of the sign array
    for (let i = 0; i < this.cellSignsArray.length; i++) {
      if (this.cellSignsArray[i] === this.sign) {
        signArray.push(i); // Push the index of the sign played to the array
      }
    }
    // Loop and check if the sign array exist in the winning
    for (let i = 0; i < this.winningArray.length; i++) {
      if (this.winningArray[i].every(elem => signArray.indexOf(elem) > -1)) {
        this.gameState = 'win'; // Update the game state
        // Update the win cell to change their class
        this.winCells[this.winningArray[i][0]] = true;
        this.winCells[this.winningArray[i][1]] = true;
        this.winCells[this.winningArray[i][2]] = true;
        this.winTriad = this.winningArray[i];
        break;
      }
    }
  }

  // Set the subscription to grab the survey from the gallowsStore
  setUpSurveySubscription() {
    this.surveyDoneSubscription = this.tictactoeStore
      .select('TicTacToeData')
      .subscribe(state => {
        state.suggestion > 0
          ? (this.answeredEnjoyQuestions = true)
          : (this.answeredEnjoyQuestions = false);
      });
  }

  // Set the next page to the introduction page
  goToIntro() {
    if (this.answeredEnjoyQuestions) {
      this.nextPage = 'intro';
    }
  }

  // Before going to the next page send the winning variables to the store
  resetGame() {
    this.tictactoeStore.dispatch(
      new TicTacToeActions.EndTGame({
        finalGameState: this.gameState,
        winningArray: this.winTriad,
      })
    );
  }
}
