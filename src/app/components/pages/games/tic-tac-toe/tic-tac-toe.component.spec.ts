import { tictactoeReducer } from 'app/state-management/reducers/tic-tac-toe.reducer';
import { JourneyService } from './../../../../services/journey.service';
import { CellComponent } from './cell/cell.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicTacToeComponent } from './tic-tac-toe.component';
import { MessageComponent } from 'app/components/shared/message/message.component';
import { NavButtonComponent } from 'app/components/shared/nav-button/nav-button.component';
import { Store, StoreModule } from '@ngrx/store';
import { TicTacToeDataStore } from 'app/state-management/state/tic-tac-toe.state';
import * as TicTacToeActions from './../../../../state-management/actions/tic-tac-toe.action';

describe('TicTacToeComponent', () => {
  let component: TicTacToeComponent;
  let fixture: ComponentFixture<TicTacToeComponent>;
  let tictactoeStore: Store<TicTacToeDataStore>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TicTacToeComponent,
        CellComponent,
        MessageComponent,
        NavButtonComponent,
      ],
      imports: [
        StoreModule.forRoot({
          TicTacToeData: tictactoeReducer,
        }),
      ],
      providers: [JourneyService, Store],
    }).compileComponents();
    // important part! connect the current jasmine store with the one in the component to call dispatch!
    tictactoeStore = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicTacToeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the text', () => {
    expect(component.textShown).toContain(' is playing now');
    component.gameState = 'win';
    component.setPlayerText();
    expect(component.textShown).toContain('Well done', 'you won');
    component.gameState = 'draw';
    component.setPlayerText();
    expect(component.textShown).toContain(
      'You are both really good! The game was a draw'
    );
  });

  it('should call click cell function', () => {
    let spyClick = spyOn(component, 'clickCell');
    let clickArea = fixture.debugElement.nativeElement.querySelector('#col');
    clickArea.click();
    expect(spyClick).toHaveBeenCalled();
  });

  it('should check that click cell updates array and changes sign', () => {
    component.round = 6;
    component.clickCell(1);
    expect(component.cellSignsArray).toContain('X');
    expect(component.sign).toBe('O');
  });

  it('should check that click cell calls win state and produces draw', () => {
    let spyWinState = spyOn(component, 'checkWinState');
    component.round = 8;
    component.clickCell(1);
    expect(spyWinState).toHaveBeenCalled();
    expect(component.gameState).toBe('draw');
  });

  it('should bring a win state', () => {
    component.cellSignsArray[0] = component.cellSignsArray[1] = component.cellSignsArray[2] =
      'X';
    component.checkWinState();
    expect(component.gameState).toBe('win');
    expect(component.winCells).toEqual([
      true,
      true,
      true,
      false,
      false,
      false,
      false,
      false,
      false,
    ]);
  });

  it('should update answerEnjoyQuestions on change', () => {
    component.answeredEnjoyQuestions = true;
    component.goToIntro();
    expect(component.nextPage).toBe('intro');
  });

  it('should set subscription', () => {
    component.setUpSurveySubscription();
    expect(component.answeredEnjoyQuestions).toEqual(false);
    tictactoeStore.dispatch(
      new TicTacToeActions.SetTEnjoy({
        yourDay: 1,
        game: 1,
        suggestion: 1,
      })
    );
    component.setUpSurveySubscription();
    expect(component.answeredEnjoyQuestions).toEqual(true);
  });

  it('should dispatch the game variables', () => {
    let storeSpy = spyOn(tictactoeStore, 'dispatch');
    component.resetGame();
    expect(storeSpy).toHaveBeenCalled();
  });
});
