import { CellComponent } from './tic-tac-toe/cell/cell.component';
import { KeyboardComponent } from './gallows/keyboard/keyboard.component';
import { SecretWordComponent } from './gallows/secret-word/secret-word.component';
import { HangmanComponent } from './gallows/hangman/hangman.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesComponent } from './games.component';
import { GameDataService } from 'app/services/game-data.service';
import { GallowsComponent } from './gallows/gallows.component';
import { TicTacToeComponent } from './tic-tac-toe/tic-tac-toe.component';
import { MessageComponent } from 'app/components/shared/message/message.component';
import { NavButtonComponent } from 'app/components/shared/nav-button/nav-button.component';

describe('GamesComponent', () => {
  let component: GamesComponent;
  let fixture: ComponentFixture<GamesComponent>;
  let expectedData = [
    {
      id: '1',
      name: 'gallows',
      component: 'gallows', //GallowsComponent,
    },
  ];
  let mockGameService = <GameDataService>{
    getGamePlayed: () => {
      return expectedData.map(nameElement => nameElement.name);
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GamesComponent,
        GallowsComponent,
        TicTacToeComponent,
        HangmanComponent,
        SecretWordComponent,
        KeyboardComponent,
        MessageComponent,
        NavButtonComponent,
        CellComponent,
      ],
      providers: [{ provide: GameDataService, useValue: mockGameService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should take the current game', () => {
    let game = mockGameService.getGamePlayed();
    expect(game).toContain('gallows');
  });
});
