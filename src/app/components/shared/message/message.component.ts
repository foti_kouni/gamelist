import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  @Input()
  messageShown: string; // The winning message that should be shown for each game

  constructor() {}

  ngOnInit() {}
}
