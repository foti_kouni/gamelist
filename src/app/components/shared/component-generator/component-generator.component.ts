import { GeneratedComponentData } from './../../../types/generated-component';
import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ComponentFactoryResolver,
} from '@angular/core';
import { PageTemplateDirective } from '../../../directives/page-template.directive';

@Component({
  selector: 'app-component-generator',
  templateUrl: './component-generator.component.html',
  styleUrls: ['./component-generator.component.scss'],
})
export class ComponentGeneratorComponent implements OnInit {
  @Input()
  gamePlayed: any; // The component that we need to generate
  @ViewChild(PageTemplateDirective)
  game: PageTemplateDirective; // Instance of the template directive we need

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver // component factory resolver injection
  ) {}

  ngAfterViewInit() {
    this.loadComponent(); // Call function to create the component after initialization
  }

  ngOnInit() {}

  // Create the component we want to generate
  loadComponent() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      this.gamePlayed
    ); // Instantiate the factory with the comonent we want to generate

    const viewContainerRef = this.game.viewContainerRef; // Take the container reference of the template

    const componentRef = viewContainerRef.createComponent(componentFactory); // create the component from the factory

    (componentRef.instance as GeneratedComponentData).data = this.gamePlayed; // assign in the data of the instance component that created the component we want to generate
  }
}
