import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
} from '@angular/core/testing';

import { NavButtonComponent } from './nav-button.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('NavButtonComponent', () => {
  let component: NavButtonComponent;
  let fixture: ComponentFixture<NavButtonComponent>;
  let mockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [NavButtonComponent],
      providers: [{ provide: Router, useValue: mockRouter }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call function on click', async(() => {
    let spyClick = spyOn(component, 'gotoPath');
    let button = fixture.debugElement.nativeElement.querySelector('input');
    button.click();
    fixture.whenStable().then(() => {
      expect(spyClick).toHaveBeenCalled();
    });
  }));

  it('should navigate and emit on function call', () => {
    component.nextPage = '/games';
    component.buttonText = 'test';
    let spyEmit = spyOn(component.navButtonActivated, 'emit');
    component.gotoPath();
    expect(spyEmit).toHaveBeenCalledWith('test');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/games']);
  });
});
