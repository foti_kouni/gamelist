import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-button',
  templateUrl: './nav-button.component.html',
  styleUrls: ['./nav-button.component.scss'],
})
export class NavButtonComponent implements OnInit {
  @Input()
  buttonText: string; // The game that should be shown on the button
  @Input()
  nextPage: string; // The next page that the button should navigate

  @Output()
  navButtonActivated: EventEmitter<string> = new EventEmitter<string>(); // transfer on father on button click the information for the game component that should be called

  constructor(
    private router: Router // Router injection for navigation in the next page
  ) {}

  ngOnInit() {}

  // Transfer in father data and navigate
  gotoPath() {
    this.navButtonActivated.emit(this.buttonText); // Emit the game that is selected so father component can call the respective element
    this.router.navigate([this.nextPage]); // Navigate to next page that father gave
  }
}
