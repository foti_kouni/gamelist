import { JourneyService } from './../../../services/journey.service';
import { GameDataService } from './../../../services/game-data.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesListComponent } from './games-list.component';
import { NavButtonComponent } from '../nav-button/nav-button.component';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';

describe('GamesListComponent', () => {
  let component: GamesListComponent;
  let navComponent: NavButtonComponent;
  let fixture: ComponentFixture<GamesListComponent>;
  let navFixture: ComponentFixture<NavButtonComponent>;
  let mockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GamesListComponent, NavButtonComponent],
      providers: [
        GameDataService,
        JourneyService,
        { provide: Router, useValue: mockRouter },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    navFixture = TestBed.createComponent(NavButtonComponent);
    navComponent = navFixture.componentInstance;
    navFixture.detectChanges();
  });

  it('should call navButtonClicked', () => {
    let navSpy = spyOn(component, 'navButtonClicked');
    let button = fixture.debugElement.nativeElement.querySelector('input');
    button.click();
    expect(navSpy).toHaveBeenCalled();
  });
});
