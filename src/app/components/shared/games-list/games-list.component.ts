import { Component, OnInit } from '@angular/core';
import { JourneyService } from './../../../services/journey.service';
import { GameDataService } from './../../../services/game-data.service';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss'],
})
export class GamesListComponent implements OnInit {
  gameNames: string[]; // variable that holds the games that exist in the model and should be implemented
  nextPage: string; // variable for the next page that should be sent in the child component

  constructor(
    private gameData: GameDataService, // Injection for the game data service
    private journey: JourneyService // Injection for th journey service
  ) {}

  ngOnInit() {
    this.gameNames = this.gameData.getGames(); // Grab the existing games from the model
    this.journey.setCurrentPage('intro'); // Set the current pge to initialize the journey
    this.nextPage = this.journey.getNextPage(); // grab next page from the journey
  }

  // Grab the button click and the game that is played from the child component
  navButtonClicked(inputGame: string) {
    this.gameData.setGamePlayed(inputGame); // Set the game that will be played
  }
}
