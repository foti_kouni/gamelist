import { CellComponent } from './components/pages/games/tic-tac-toe/cell/cell.component';
import { GallowsWordsService } from './services/gallows-words.service';
import { JourneyService } from './services/journey.service';
import { GameDataService } from './services/game-data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { IntroComponent } from './components/pages/intro/intro.component';
import { NavButtonComponent } from './components/shared/nav-button/nav-button.component';
import { GallowsComponent } from './components/pages/games/gallows/gallows.component';
import { GamesComponent } from './components/pages/games/games.component';
import { PageTemplateDirective } from './directives/page-template.directive';
import { ComponentGeneratorComponent } from './components/shared/component-generator/component-generator.component';
import { TicTacToeComponent } from './components/pages/games/tic-tac-toe/tic-tac-toe.component';
import { SecretWordComponent } from './components/pages/games/gallows/secret-word/secret-word.component';
import { StoreModule } from '@ngrx/store';
import { gallowsReducer } from './state-management/reducers/gallows.reducer';
import { KeyboardComponent } from './components/pages/games/gallows/keyboard/keyboard.component';
import { HangmanComponent } from './components/pages/games/gallows/hangman/hangman.component';
import { GamesListComponent } from './components/shared/games-list/games-list.component';
import { EnjoyMeterComponent } from './components/pages/enjoy-meter/enjoy-meter.component';
import { tictactoeReducer } from './state-management/reducers/tic-tac-toe.reducer';
import { MessageComponent } from './components/shared/message/message.component';

const appRoutes: Routes = [
  { path: 'intro', component: IntroComponent },
  { path: 'games', component: GamesComponent },
  { path: 'enjoy-meter', component: EnjoyMeterComponent },
  { path: '**', component: IntroComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    NavButtonComponent,
    GallowsComponent,
    GamesComponent,
    PageTemplateDirective,
    ComponentGeneratorComponent,
    TicTacToeComponent,
    SecretWordComponent,
    KeyboardComponent,
    HangmanComponent,
    GamesListComponent,
    EnjoyMeterComponent,
    CellComponent,
    MessageComponent,
  ],
  entryComponents: [GallowsComponent, TicTacToeComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    StoreModule.forRoot({
      gallowsData: gallowsReducer,
      TicTacToeData: tictactoeReducer,
    }),
  ],
  providers: [GameDataService, JourneyService, GallowsWordsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
