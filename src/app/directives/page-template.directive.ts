import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appPageTemplate]',
})
export class PageTemplateDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
