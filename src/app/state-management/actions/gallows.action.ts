import { Action } from '@ngrx/store';

export const ADD_WORD = '[GALLOWS] Add chosen word';
export const ADD_LETTER = '[GALLOWS] Add selected letter';
export const END_GAME = '[GALLOWS] End game';
export const RESET_GAME = '[GALLOWS] Reset game';
export const SET_ENJOY = '[GALLOWS] Set enjoy';

export class AddChosenWord implements Action {
  readonly type = ADD_WORD;
  constructor(public payload: { word: string }) {}
}

export class AddSelectedLetter implements Action {
  readonly type = ADD_LETTER;
  constructor(public payload: { letter: string }) {}
}

export class EndGame implements Action {
  readonly type = END_GAME;
  constructor(public payload: { gameEnded: boolean }) {}
}

export class ResetGame implements Action {
  readonly type = RESET_GAME;
  constructor(
    public payload: { word: string; letters: string[]; gameEnded: boolean }
  ) {}
}
export class SetEnjoy implements Action {
  readonly type = SET_ENJOY;
  constructor(
    public payload: { yourDay: number; game: number; suggestion: number }
  ) {}
}

export type All =
  | AddChosenWord
  | AddSelectedLetter
  | EndGame
  | ResetGame
  | SetEnjoy;
