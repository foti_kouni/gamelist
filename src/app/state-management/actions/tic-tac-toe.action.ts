import { Action } from '@ngrx/store';

export const END_T_GAME = '[TICTACTOE] End tic-tac-toe game';
export const SET_T_ENJOY = '[TICTACTOE] Set enjoy';

export class EndTGame implements Action {
  readonly type = END_T_GAME;
  constructor(
    public payload: { finalGameState: string; winningArray: number[] }
  ) {}
}

export class SetTEnjoy implements Action {
  readonly type = SET_T_ENJOY;
  constructor(
    public payload: { yourDay: number; game: number; suggestion: number }
  ) {}
}

export type All = EndTGame | SetTEnjoy;
