export interface IGallowsState {
  word: string;
  letters: string[];
  gameEnded: boolean;
  yourDay: number;
  game: number;
  suggestion: number;
}

export const initialState: IGallowsState = {
  word: '',
  letters: [],
  gameEnded: false,
  yourDay: 0,
  game: 0,
  suggestion: 0,
};

export interface GallowsDataStore {
  gallowsData: IGallowsState;
}
