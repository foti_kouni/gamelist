export interface ITicTacToeState {
  finalGameState: string;
  winningArray: number[];
  yourDay: number;
  game: number;
  suggestion: number;
}

export const initialState: ITicTacToeState = {
  finalGameState: '',
  winningArray: [],
  yourDay: 0,
  game: 0,
  suggestion: 0,
};

export interface TicTacToeDataStore {
  TicTacToeData: ITicTacToeState;
}
