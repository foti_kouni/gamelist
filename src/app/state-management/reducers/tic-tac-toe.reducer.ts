import { ITicTacToeState, initialState } from './../state/tic-tac-toe.state';
import * as TicTacToeActions from './../actions/tic-tac-toe.action';

export type Action = TicTacToeActions.All;

export function tictactoeReducer(
  state: ITicTacToeState = initialState,
  action: Action
) {
  switch (action.type) {
    case TicTacToeActions.END_T_GAME:
      return {
        ...state,
        finalGame: action.payload.finalGameState,
        winningArray: action.payload.winningArray,
      };

    case TicTacToeActions.SET_T_ENJOY:
      return {
        ...state,
        yourDay: action.payload.yourDay,
        game: action.payload.game,
        suggestion: action.payload.suggestion,
      };
    default:
      return state;
  }
}
