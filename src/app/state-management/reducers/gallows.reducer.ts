import * as GallowsActions from './../actions/gallows.action';
import { IGallowsState, initialState } from '../state/gallows.state';

export type Action = GallowsActions.All;

export function gallowsReducer(
  state: IGallowsState = initialState,
  action: Action
) {
  switch (action.type) {
    case GallowsActions.ADD_WORD:
      return { ...state, word: action.payload.word };
    case GallowsActions.ADD_LETTER:
      return { ...state, letters: [...state.letters, action.payload.letter] };
    case GallowsActions.END_GAME:
      return { ...state, gameEnded: action.payload.gameEnded };
    case GallowsActions.RESET_GAME:
      return {
        ...state,
        word: action.payload.word,
        letters: action.payload.letters,
        gameEnded: action.payload.gameEnded,
      };
    case GallowsActions.SET_ENJOY:
      return {
        ...state,
        yourDay: action.payload.yourDay,
        game: action.payload.game,
        suggestion: action.payload.suggestion,
      };
    default:
      return state;
  }
}
